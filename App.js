import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import { Provider } from 'react-redux';
import { pStore, store } from './src/redux/store';
import { PersistGate } from 'redux-persist/integration/react';

import SplashScreen from './src/screens/SplashScreen';
import LoginContainer from './src/screens/AuthScreens/LoginScreen';
import RegisterContainer from './src/screens/AuthScreens/RegisterScreen';
import HomeScreen from './src/screens/HomeScreen';
import MovieScreen from './src/screens/MovieScreen';


class App extends Component {
  

  render() {
    function AuthStack() {
      return (
        <Stack.Navigator initialRouteName="Login" screenOptions={{headerShown: false}}>
          <Stack.Screen name="Login" component={LoginContainer} />
          <Stack.Screen name="Register" component={RegisterContainer} />
        </Stack.Navigator>
      );
    }


    function MainStack() {
      return (
        <Tab.Navigator initialRouteName="Home">
          <Tab.Screen name="Home" component={HomeScreen} />
          <Tab.Screen name="Movie" component={MovieScreen} />
        </Tab.Navigator>
      );
    }

    const Tab = createMaterialBottomTabNavigator();
    const Stack = createStackNavigator();
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={pStore}>
          <NavigationContainer>
            <Stack.Navigator initialRouteName="Splash" screenOptions={{headerShown: false}}>
              <Stack.Screen name="Splash" component={SplashScreen}/>
              <Stack.Screen name="AuthStack" component={AuthStack}/>
              <Stack.Screen name="MainStack" component={MainStack}/>
            </Stack.Navigator>
          </NavigationContainer>
        </PersistGate>
      </Provider>
    )
  }
}

export default App;
