import { IS_LOADING, FAIL_LOGIN } from '../actions/constants';
import { persistUserAction }  from '../../../redux/actions/actions';
import  axios  from 'axios';
import {isValidPattern, errInput} from '../../../components/RegExp';
import { ToastAndroid } from "react-native";

const setLoadingAction = isLoad => {
    return {
        type: IS_LOADING,
        data: isLoad
    }
}

const setFailLoginAction = (errorMessage) => {
    return {
        type: FAIL_LOGIN,
        data: {
            errorMessage
        }
    }
}

const validate = (loginData) =>{
    return new Promise((resolve, reject)=> {
        let isValid = true;
        for(let key in loginData) {
            console.log(key, loginData[key])
            if(isValidPattern(loginData[key].value, key)==false){
                isValid= false;
                reject({
                    message: errInput[key]
                });
            }
        }
        isValid? resolve("valid"): null
    })
}

const loginApi = (loginData)=> {
    const api = axios.create({
        baseURL: 'http://localhost:8080/api',
        headers: {
            'content-type': 'application/json'
        },
        responseType: "json",
        timeout: 2000
    })

    const data = {
        email: loginData.email.value.toLowerCase(),
        password: loginData.password.value 
    }
    return new Promise((resolve, reject) => {
        api.post("/login", data)
            .then(res=>{
                resolve(res.data);
            })
            .catch(err=>{
                if(err.code === 'ECONNABORTED'){
                    reject('timeout')
                }else{
                    reject(err.response.data)
                }
            })
    })
}


export const login = (loginData, navigation) => {
    return (
        (dispatch)=> {
            dispatch(setLoadingAction(true));
            Promise.all([validate(loginData), loginApi(loginData)])
                .then(res=> {
                    dispatch(persistUserAction(res[1])); 
                    navigation.replace("MainStack");
                }).catch(err=> {
                    if (err=='timeout'){
                        ToastAndroid.show("Connection timeout. please try again", ToastAndroid.SHORT);
                    }else{
                        dispatch(setFailLoginAction(err.message))
                    }
                }).finally(()=>{
                    dispatch(setLoadingAction(false));
                })
        }
    )
}


