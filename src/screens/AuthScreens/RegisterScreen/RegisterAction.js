import { IS_LOADING, FAIL_REGISTER } from '../actions/constants';
import { persistUserAction }  from '../../../redux/actions/actions';
import  axios  from 'axios';
import {isValidPattern, errInput} from '../../../components/RegExp';
import { Alert, ToastAndroid } from "react-native";
import { resetAction, setLoginValueAction } from '../actions/actions';

const setLoadingAction = isLoad => {
    return {
        type: IS_LOADING,
        data: isLoad
    }
}

const setFailRegisterAction = (errorMessage) => {
    return {
        type: FAIL_REGISTER,
        data: {
            errorMessage
        }
    }
}

const validate = (data) =>{
    return new Promise((resolve, reject)=> {
        let isValid = true;
        for(let key in data) {
            console.log(key, data[key])
            if (key== 'confirmPassword'){
                if(data[key].value != data.password.value){
                    isValid = false;
                    reject({
                        message: errInput[key]
                    })
                }
            }
            else if(isValidPattern(data[key].value, key)==false){
                isValid= false;
                reject({
                    message: errInput[key]
                });
            }
            
        }
        isValid? resolve("valid"): null
    })
}

const registerApi = (registerData)=> {
    const api = axios.create({
        baseURL: 'http://localhost:8080/api',
        headers: {
            'content-type': 'application/json'
        },
        responseType: "json",
        timeout: 2000
    })

    const data = {
        email: registerData.email.value.toLowerCase(),
        name: registerData.fullName.value,
        password: registerData.password.value,
        userName: registerData.userName.value
    }
    return new Promise((resolve, reject) => {
        api.post("/register", data)
            .then(res=>{
                console.log("res",res)
                resolve(res.data);
            })
            .catch(err=>{
                if(err.code === 'ECONNABORTED'){
                    reject('timeout')
                }else{
                    console.log(err);
                    reject(err.response.data)
                }
            })
    })
}


export const register = (registerData, navigation) => {
    console.log("in register block")
    return (
        (dispatch)=> {
            dispatch(setLoadingAction(true));
            Promise.all([validate(registerData), registerApi(registerData)])
                .then(res=> {
                    let resData = res[1];
                    if(resData.status==200){
                        ToastAndroid.show(resData.message, ToastAndroid.SHORT);
                        dispatch(resetAction());
                        dispatch(setLoginValueAction("email", registerData.email.value, registerData.email.status))
                        
                        navigation.navigate("Login") 
                    }   
                    // dispatch(setLoginValueAction())              
                }).catch(err=> {
                    if (err=='timeout'){
                        ToastAndroid.show("Connection timeout. please try again", ToastAndroid.SHORT);
                    }else{
                        console.log(err)
                        dispatch(setFailRegisterAction(err.message))
                    }
                }).finally(()=>{
                    dispatch(setLoadingAction(false));
                })
            
        }
    )
}


