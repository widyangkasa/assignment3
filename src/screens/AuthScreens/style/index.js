export const AuthStyle = (width,height) => ({
    container: {
        flex:1,
        width: width,
        backgroundColor: 'darkred',
        alignItems: 'center',
        justifyContent:'center'
    },
    headerBg: {
        position:'absolute',
        width: height,
        height: height,
        backgroundColor: "white",
        borderRadius:1000/2,
        top: -height*0.75,
        right: width*0.05
    },
    headerLogoWrapper: {
        width: width*0.5,
        alignItems:'center',
        justifyContent:'center',
        height: height*0.12,
        position:'absolute',
        top: 20,
        left: 15
    },
    headerLogo:{
        flex: 1,
        resizeMode:'contain'
    },
    formWrapper:{
        width: width*0.8,
        height: height*0.7,
        position: 'absolute',
        backgroundColor: '#16275D',
        right: 0,
        top: height*0.17,
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
        justifyContent:'flex-start',
        padding: 20,
        shadowColor: "#16275D",
        shadowOffset: {
            width: 0,
            height: 11,
        },
        shadowOpacity: 0.57,
        shadowRadius: 15.19,        
        elevation: 16
    },
    formHeader:{
        fontSize: 25,
        color: 'white',
        marginBottom: 20
    },
    submitBtnWrapper: {
        width: width*0.75,
        alignItems: 'center',
        position:'absolute',
        // backgroundColor:'pink',
        bottom: -20
    },
    submitBtn:{
        width: 150,
        height: 38,
        backgroundColor: 'white',
        borderRadius: 30,
        justifyContent: 'center',
        alignItems:'center'
    },
    linkWrapper:{
        marginTop: 10,
        flexDirection: 'row',
    },
    linkDesc:{
        color: 'white',
    } ,
    link:{
        color: 'lightblue',
        fontWeight:'bold'
    },
    loadCover: {
        position: 'absolute',
        width: width,
        height: height,
        backgroundColor: 'white',
        opacity: 0.2,
        elevation:20
    },
    errorMessage: {
        color: 'red'
    }

})
