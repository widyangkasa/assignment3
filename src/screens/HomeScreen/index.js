import React, {Component} from 'react';
import { StyleSheet, View, Text, Button} from 'react-native';

import {connect} from 'react-redux';
import { logout } from '../../redux/actions/actions';

class HomeScreen extends Component{
    render() {
        return(
            <View>
                <Text>Welcome, {this.props.user.name}</Text>
                <Text>{JSON.stringify(this.props.global)}</Text>
                <Button title="Logout" onPress={()=>this.props.logout(this.props.navigation)}/>
            </View>
            
        )
    }
}

const mapStateToProps=(state) => {
    return{
        user: state.user,
        global: state
    }
}

const mapDispatchToProps=(dispatch)=> {
    return{
        logout: (navigation)=> dispatch(logout(navigation))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);