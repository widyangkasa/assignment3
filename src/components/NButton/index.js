import React, {Component} from 'react';
import {TouchableOpacity, ActivityIndicator} from 'react-native';

class NButton extends Component{

    render() {
        return(
            <TouchableOpacity style={this.props.style} onPress={this.props.onPress}>
                {
                this.props.loading?
                (
                    <ActivityIndicator size="small" color="#0000ff"/>
                ):
                (this.props.children)
                }
            </TouchableOpacity>
        )
    }
}

export default NButton;