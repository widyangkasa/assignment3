import React, {Component} from 'react';
import { StyleSheet, View, TextInput, Dimensions} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const {height, width} = Dimensions.get('window');
const validationIconName = {
    valid: 'check',
    invalid: 'times'
}

class NInputText extends Component{

    state={
        secureTextEntry: true,
        eyeType: false
    }

    changeEye = () => {
        this.setState({
            secureTextEntry: !this.state.secureTextEntry,
            eyeType: !this.state.eyeType
        })
    }
    
    render() {
        const {placeholder, iconName, onChangeText, inputName ,inputType, iconSize, maxLength, status, value}    = this.props;
        const inputStyle = {
            color: `${this.props.colors}`,
            width: this.props.width
        }



        return(
            <View style={styles(inputStyle).container}>
            {
                inputType!=='password'? ( <TextInput placeholder={placeholder} onChangeText={(text) => onChangeText(text, inputName)} style={styles(inputStyle).inputText}
                value={value}/>): 
                (
                    <View>
                    <TextInput placeholder={placeholder} onChangeText={(text) => onChangeText(text, inputName)} style={styles(inputStyle).inputText}
                    secureTextEntry={this.state.secureTextEntry} maxLength={maxLength} value={value}/>
                    
                        <FontAwesome5 
                        solid
                        name={this.state.eyeType==false?'eye-slash': 'eye'}
                        size={iconSize}
                        style={styles(inputStyle).inputIconEye}
                        onPress={() => this.changeEye()}/>
                    </View>
                )

             }
               
                <FontAwesome5 
                    solid
                    name={iconName}
                    size={iconSize}
                    onPress={() => alert('hello')} style={styles(inputStyle).inputIcon}/>
                {
                    status!== '' || status!== 'loading'? (
                    <FontAwesome5 
                        name={validationIconName[status]}
                        size={iconSize}
                        onPress={() => alert('hello')} style={styles(inputStyle)[status]}/>   
                    ):null
                }
                
            </View>
        )
    }
}

const styles = (inputStyle) => StyleSheet.create({
    container: {
        marginTop: 5,
        marginBottom: 5,
        flexDirection:'row'
    },
    inputText: {
        width: inputStyle.width,
        color: 'white',
        backgroundColor: '#233776',
        // borderColor: inputStyle.color,
        // borderWidth: 2,
        borderRadius: 30,
        paddingLeft: 50,
        paddingRight: 20,
        paddingTop: 5,
        paddingBottom: 5,
    },
    inputIcon: {
        color: inputStyle.color,
        position:'absolute',
        top: 8,
        left: 20
    },
    inputIconEye:{
        position:'absolute',
        top: 8,
        right: 20,
        color: inputStyle.color,
        opacity: 0.5
    },
    valid: {
        top: 8,
        left: 10,
        color: 'green'
    },
    invalid: {
        top: 8,
        left: 10,
        color: 'red',

    }
    
})

NInputText.defaultProps={
    colors: 'grey',
    width: width*0.8,
    iconSize: 20
    
}
export default NInputText;