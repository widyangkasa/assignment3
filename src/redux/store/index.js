import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist'
import { reducers } from '../reducers';
import AsyncStorage from '@react-native-async-storage/async-storage';

const pReducer = persistReducer({
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['user']
},reducers);

const store = createStore(pReducer, applyMiddleware(thunk))
const pStore = persistStore(store)

export {store, pStore};