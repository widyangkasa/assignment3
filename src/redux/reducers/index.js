import { loginReducer } from '../../screens/AuthScreens/LoginScreen/LoginReducer';
import { registerReducer } from '../../screens/AuthScreens/RegisterScreen/RegisterReducer';
import { userReducer } from './userReducer';
import { combineReducers } from 'redux';

export const reducers = combineReducers({
    login: loginReducer,
    register: registerReducer,
    user: userReducer
})