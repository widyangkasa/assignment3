import { PERSIST_USER, CLEAR_PERISISTED_USER } from '../actions/constants';

const initialState = {}

export const userReducer = (state = initialState, action) => {
    switch(action.type){
        case PERSIST_USER:
            return {
                ...action.data.user
            }
        case CLEAR_PERISISTED_USER: 
            return initialState
        default: 
            return state;
    }
}