import { PERSIST_USER, CLEAR_PERISISTED_USER } from './constants';

export const persistUserAction = (user) => {
    return {
        type: PERSIST_USER,
        data: {
            user
        }
    }
}

const clearPersistedUserAction = () => {
    return {
        type: CLEAR_PERISISTED_USER
    }

}

export const logout = (navigation) => {
    return (dispatch)=>{
        dispatch(clearPersistedUserAction());
        navigation.replace("AuthStack");
    }
}

